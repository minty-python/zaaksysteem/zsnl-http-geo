# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from unittest import TestCase, mock
from zsnl_http_geo.__init__ import log_record_factory
from zsnl_http_geo.views.geo_feature import GeoFeatureViews


class TestGeoFeature(TestCase):
    """
    Class containing main tests for the ZSNL HTTP Geo Service
    """

    def test_GeoFeature(self):
        """
        Tests for the presence and accuracy of content within the GeoFeature View Class.
        """
        view = GeoFeatureViews(mock.MagicMock(), mock.MagicMock())

        # Correct instance
        self.assertIsInstance(view, GeoFeatureViews)

        # Correct class-variables
        self.assertTrue(hasattr(view, "create_link_from_entity"))
        self.assertTrue(hasattr(view, "view_mapper"))
        self.assertIsNone(view.create_link_from_entity(mock.MagicMock()))

    def test_log_record_factory(self):
        """
        Testing the log record factory
        """
        record = log_record_factory(
            name="foo",
            level="WARN",
            pathname="/dev/null",
            lineno=42,
            args={},
            msg="bar",
            exc_info=None,
        )

        assert record.msg == "bar"
        assert record.zs_component == "zsnl_http_geo"
